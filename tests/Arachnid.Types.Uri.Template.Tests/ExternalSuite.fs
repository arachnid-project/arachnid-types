module Arachnid.Types.Uri.Templates.Tests.ExternalSuite

open Arachnid.Types.Tests
open Arachnid.Types.Tests.Operators
open Arachnid.Types.Uri.Template
open Chiron
open Chiron.Inference
open Chiron.Operators
module D  = Chiron.Serialization.Json.Decode
module DI = Chiron.Inference.Json.Decode
module JI = Chiron.Inference.Json
open System.IO
open System.Reflection
open Xunit
open System


type TestInput = string


type TestResult =
  | SingleResult of string
  | MultipleResults of string list
  | ParseFailure
  with
    static member FromJson (_ : TestResult) =
      D.oneOf [ SingleResult <!> D.string
                MultipleResults <!> D.stringList
                D.bool >=> (fun b -> JsonResult.pass ParseFailure)
              ]

let validateResult =
  function
  | SingleResult (s) ->
    function
    | Ok(r) -> r =? s
    | Error(e) -> failwith (sprintf "Expected proper template rendering to %s, but had error %O" s e)
  | MultipleResults (l) ->
    function
    | Ok(r) -> l |?| r
    | Error(e) -> failwith (sprintf "Expected proper template rendering to match %O, but had error %O" l e)
  | ParseFailure ->
    function
    | Ok(r) -> failwith (sprintf "Expected template rendering failure but got %s" r)
    | Error(_) -> ()

type TestCase = (TestInput * TestResult)

let decodeKey = Key >> JsonResult.pass

let decodeValue =
    D.oneOf [ Atom >> Some <!> D.string
              string >> Atom >> Some <!> D.int
              string >> Atom >> Some <!> D.float
              List >> Some <!> D.stringList
              (Seq.map (fun (KeyValue (k, v)) -> (k,v)) >> List.ofSeq >> Keys >> Some) <!> D.mapWith D.string
              (fun _ -> None) <!> D.unit
            ]

let decodeData =
      Map.filter (fun _k -> Option.isSome) >> Map.map (fun _k -> Option.get) >> UriTemplateData
  <!> D.mapWithCustomKey decodeKey decodeValue

type Example =
  { Level : int
  ; Variables : UriTemplateData
  ; TestCases : TestCase list
  } with
  static member FromJson (_ : Example) = jsonDecoder {
    let! level = D.optional D.int "level"
    let! vars = D.required decodeData "variables"
    let! tests = DI.required "testcases"

    return { Level = Option.defaultValue 4 level
           ; Variables = vars
           ; TestCases = tests
           }
  }

type TestSuite = Map<string, Example>

type SuiteLoader =
  static member loadSuite (filename : String) = seq {
    use stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(sprintf "Arachnid.Types.Uri.Template.Tests.data.%s.json" filename)
    use reader = new StreamReader(stream)
    let suite : TestSuite =
      reader.ReadToEnd()
      |> Json.deserialize
      |> JsonResult.getOrThrow

    for KeyValue (name, s) in suite do
      for input, result in s.TestCases do
        yield
          [|box filename
          ; box name
          ; box input
          ; box s.Variables
          ; box result
          |]
  }

[<Theory>]
[<MemberData(memberName = "loadSuite", parameters = [| "spec-examples" |], MemberType = typedefof<SuiteLoader>)>]
[<MemberData(memberName = "loadSuite", parameters = [| "spec-examples-by-section" |], MemberType = typedefof<SuiteLoader>)>]
[<MemberData(memberName = "loadSuite", parameters = [| "extended-tests" |], MemberType = typedefof<SuiteLoader>)>]
[<MemberData(memberName = "loadSuite", parameters = [| "negative-tests" |], MemberType = typedefof<SuiteLoader>)>]
let ``URI Template rendering work matches spec`` (_filename : string, _example : string, template: string, data : UriTemplateData, result : TestResult) =
  template
  |> UriTemplate.tryParse
  |> Result.bind (Rendering.tryRender UriTemplate.Rendering.Render data)
  |> validateResult result

[<Theory(Skip ="Handle length suffix issue")>]
[<MemberData(memberName = "loadSuite", parameters = [| "spec-examples" |], MemberType = typedefof<SuiteLoader>)>]
let ``URI Template matching passes spec`` (_filename : String, _example : String, template: String, data : UriTemplateData, result : TestResult) =
  let t = UriTemplate.parse template
  match result with
  | SingleResult(s) ->
    match t.Match s with
    | UriTemplateData (matches) ->
      for KeyValue (k,v) in matches do
        let expectedValue = Map.tryFind k ((fst UriTemplateData.uriTemplateData_) data)

        expectedValue =? Some v
  | MultipleResults (l) -> ()
  | _ -> ()

