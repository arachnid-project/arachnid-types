﻿module Arachnid.Types.Http.Tests

open System
open Arachnid.Types.Http
open Arachnid.Types.Language
open Arachnid.Types.Tests
open Arachnid.Types.Uri
open Xunit

(* RFC 7230 *)

[<Fact>]
let ``PartialUri Formatting/Parsing`` () =

    let partialUriTyped : PartialUri =
        PartialUri (
            RelativePart.Absolute (PathAbsolute [ "some"; "path" ]),
            Some (Query "key=val"))

    let partialUriString =
        "/some/path?key=val"

    roundTrip (PartialUri.format, PartialUri.parse) [
        partialUriTyped, partialUriString ]

[<Fact>]
let ``HttpVersion Formatting/Parsing`` () =

    let httpVersionTyped = HTTP 1.1
    let httpVersionString = "HTTP/1.1"

    let http2VersionTyped = HTTP 2.0
    let http2VersionString = "HTTP/2.0"

    roundTrip (HttpVersion.format, HttpVersion.parse) [
        httpVersionTyped, httpVersionString ;
        http2VersionTyped, http2VersionString ]

[<Fact>]
let ``ContentLength Formatting/Parsing`` () =

    let contentLengthTyped = ContentLength 1024
    let contentLengthString = "1024"

    roundTrip (ContentLength.format, ContentLength.parse) [
        contentLengthTyped, contentLengthString ]

[<Fact>]
let ``Host Formatting/Parsing`` () =

    let hostTyped = Host (Name (RegName "www.example.com"), Some (Port 8080))
    let hostString = "www.example.com:8080"

    roundTrip (Host.format, Host.parse) [
        hostTyped, hostString ]

[<Fact>]
let ``Connection Formatting/Parsing`` () =

    let connectionTyped = Connection ([ ConnectionOption "close"; ConnectionOption "test" ])
    let connectionString = "close,test"

    roundTrip (Connection.format, Connection.parse) [
        connectionTyped, connectionString ]

(* RFC 7231 *)

[<Fact>]
let ``MediaType Formatting/Parsing`` () =

    let mediaTypeTyped =
        MediaType (
            Type "application",
            SubType "json",
            Parameters (Map.ofList [ "charset", "utf-8" ]))

    let mediaTypeString =
        "application/json;charset=utf-8"

    roundTrip (MediaType.format, MediaType.parse) [
        mediaTypeTyped, mediaTypeString ]

[<Fact>]
let ``ContentType Formatting/Parsing`` () =

    let contentTypeTyped =
        ContentType (
            MediaType (
                Type "application",
                SubType "json",
                Parameters (Map.ofList [ "charset", "utf-8" ])))

    let contentTypeString =
        "application/json;charset=utf-8"

    roundTrip (ContentType.format, ContentType.parse) [
        contentTypeTyped, contentTypeString ]

[<Fact>]
let ``ContentEncoding Formatting/Parsing`` () =

    let contentEncodingTyped =
        ContentEncoding [
            ContentCoding "compress"
            ContentCoding "deflate" ]

    let contentEncodingString =
        "compress,deflate"

    roundTrip (ContentEncoding.format, ContentEncoding.parse) [
        contentEncodingTyped, contentEncodingString ]

[<Fact>]
let ``ContentLanguage Formatting/Parsing`` () =

    let contentLanguageTyped =
        ContentLanguage [
            LanguageTag (
                Language ("en", None),
                None,
                Some (Region "GB"),
                Variant [])
            LanguageTag (
                Language ("hy", None),
                Some (Script "Latn"),
                Some (Region "IT"),
                Variant [ "arvela" ]) ]

    let contentLanguageString =
        "en-GB,hy-Latn-IT-arvela"

    roundTrip (ContentLanguage.format, ContentLanguage.parse) [
        contentLanguageTyped, contentLanguageString ]

[<Fact>]
let ``ContentLocation Formatting/Parsing`` () =

    let contentLocationTyped =
        ContentLocation (
            ContentLocationUri.Absolute (
                AbsoluteUri (
                    Scheme "http",
                    HierarchyPart.Absolute (PathAbsolute [ "some"; "path" ]),
                    None)))

    let contentLocationString =
        "http:/some/path"

    roundTrip (ContentLocation.format, ContentLocation.parse) [
        contentLocationTyped, contentLocationString ]

[<Fact>]
let ``Product Formatting/Parsing`` () =

    roundTrip (Product.format, Product.parse) [
        Product("Apache", Some(ProductVersion("1.1"))), "Apache/1.1"
        Product("Apache", None), "Apache" ]

[<Fact>]
let ``Method Formatting/Parsing`` () =

    roundTrip (Method.format, Method.parse) [
        Method.GET, "GET"
        Method.Custom "PATCH", "PATCH" ]

[<Fact>]
let ``UserAgent Formatting/Parsing`` () =
    let userAgentTyped =
        UserAgent [
            CommentedProduct(Product("CERN-LineMode", Some(ProductVersion("2.15"))), [])
            CommentedProduct(Product("libwww", Some(ProductVersion("2.17b3"))), []) ]

    let userAgentString =
        "CERN-LineMode/2.15 libwww/2.17b3"

    let chromeUserAgentTyped =
        UserAgent [
            CommentedProduct(Product("Mozilla", Some(ProductVersion("5.0"))), ["X11; Linux x86_64"])
            CommentedProduct(Product("AppleWebKit", Some(ProductVersion("537.36"))), ["KHTML, like Gecko"])
            CommentedProduct(Product("Chrome", Some(ProductVersion("51.0.2704.103"))), [])
            CommentedProduct(Product("Safari", Some(ProductVersion("537.36"))), [])
        ]

    let chromeUserAgentString =
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"

    roundTrip (UserAgent.format, UserAgent.parse) [
        userAgentTyped, userAgentString
        chromeUserAgentTyped, chromeUserAgentString ]

let ``Server Formatting/Parsing`` () =
    let serverTyped =
        Server [
            CommentedProduct(Product("CERN-LineMode", Some(ProductVersion("2.15"))), [])
            CommentedProduct(Product("libwww", Some(ProductVersion("2.17b3"))), []) ]

    let serverString =
        "CERN-LineMode/2.15 libwww/2.17b3"

    roundTrip (Server.format, Server.parse) [
        serverTyped, serverString ]

[<Fact>]
let ``Expect Formatting/Parsing`` () =

    roundTrip (Expect.format, Expect.parse) [
        Expect (Continue), "100-continue" ]

[<Fact>]
let ``MaxForwards Formatting/Parsing`` () =

    roundTrip (MaxForwards.format, MaxForwards.parse) [
        MaxForwards 10, "10" ]

[<Fact>]
let ``Accept Formatting/Parsing`` () =

    let acceptTyped =
        Accept [
            AcceptableMedia (
                Closed (Type "application", SubType "json", Parameters Map.empty),
                Some (AcceptParameters (Weight 0.7, Extensions Map.empty)))
            AcceptableMedia (
                MediaRange.Partial (Type "text", Parameters Map.empty),
                Some (AcceptParameters (Weight 0.5, Extensions Map.empty)))
            AcceptableMedia (
                Open (Parameters Map.empty),
                None) ]

    let acceptString =
        "application/json;q=0.7,text/*;q=0.5,*/*"

    roundTrip (Accept.format, Accept.parse) [
        acceptTyped, acceptString ]

[<Fact>]
let ``AcceptCharset Formatting/Parsing`` () =

    let acceptCharsetTyped =
        AcceptCharset [
            AcceptableCharset (
                CharsetRange.Charset (Charset.Utf8),
                Some (Weight 0.7))
            AcceptableCharset (
                CharsetRange.Any,
                Some (Weight 0.2)) ]

    let acceptCharsetString =
        "utf-8;q=0.7,*;q=0.2"

    roundTrip (AcceptCharset.format, AcceptCharset.parse) [
        acceptCharsetTyped, acceptCharsetString ]

[<Fact>]
let ``AcceptEncoding Formatting/Parsing`` () =

    let acceptEncodingTyped =
        AcceptEncoding [
            AcceptableEncoding (
                EncodingRange.Coding (ContentCoding.Compress),
                Some (Weight 0.8))
            AcceptableEncoding (
                EncodingRange.Identity,
                None)
            AcceptableEncoding (
                EncodingRange.Any,
                Some (Weight 0.3)) ]

    let acceptEncodingString =
        "compress;q=0.8,identity,*;q=0.3"

    roundTrip (AcceptEncoding.format, AcceptEncoding.parse) [
        acceptEncodingTyped, acceptEncodingString ]

[<Fact>]
let ``AcceptLanguage Formatting/Parsing`` () =

    let acceptLanguageTyped =
        AcceptLanguage [
            AcceptableLanguage (
                Range [ "en"; "GB" ],
                Some (Weight 0.8))
            AcceptableLanguage (
                Any,
                None) ]

    let acceptLanguageString =
        "en-GB;q=0.8,*"

    roundTrip (AcceptLanguage.format, AcceptLanguage.parse) [
        acceptLanguageTyped, acceptLanguageString ]

[<Fact>]
let ``Referer Formatting/Parsing`` () =

    let refererTyped =
        Referer (
            RefererUri.Partial (
                PartialUri (
                    RelativePart.Absolute (PathAbsolute ["some"; "path" ]),
                    None)))

    let refererString =
        "/some/path"

    roundTrip (Referer.format, Referer.parse) [
        refererTyped, refererString ]

[<Fact>]
let ``Date Formatting/Parsing`` () =

    let dateTyped =
        Date.Date (DateTime.Parse ("1994/10/29 19:43:31"))

    let dateString =
        "Sat, 29 Oct 1994 19:43:31 GMT"

    roundTrip (Date.format, Date.parse) [
        dateTyped, dateString ]

[<Fact>]
let ``Location Formatting/Parsing`` () =

    let locationTyped =
        Location (
            UriReference.Uri (
                Uri.Uri (
                    Scheme "http",
                    HierarchyPart.Absolute (PathAbsolute [ "some"; "path" ]),
                    None,
                    None)))

    let locationString =
        "http:/some/path"

    roundTrip (Location.format, Location.parse) [
        locationTyped, locationString ]

[<Fact>]
let ``RetryAfter Formatting/Parsing`` () =

    let retryAfterTyped =
        RetryAfter (Delay (TimeSpan.FromSeconds (float 60)))

    let retryAfterString =
        "60"

    roundTrip (RetryAfter.format, RetryAfter.parse) [
        retryAfterTyped, retryAfterString ]

[<Fact>]
let ``Vary Formatting/Parsing`` () =

    let varyAsteriskTyped =
        Vary(Asterisk)

    let varyAsteriskString =
        "*"

    let varyFieldsTyped =
        Vary(FieldNames(["accept-language"; "accept-encoding"]))

    let varyFieldsString =
        "accept-language,accept-encoding"

    roundTrip (Vary.format, Vary.parse) [
        varyAsteriskTyped, varyAsteriskString ;
        varyFieldsTyped, varyFieldsString ]

[<Fact>]
let ``Allow Formatting/Parsing`` () =

    let allowTyped =
        Allow [ DELETE; Method.Custom "PATCH"; GET; POST; PUT ]

    let allowString =
        "DELETE,PATCH,GET,POST,PUT"

    roundTrip (Allow.format, Allow.parse) [
        allowTyped, allowString ]

(* RFC 7232 *)

[<Fact>]
let ``LastModified Formatting/Parsing`` () =

    let lastModifiedTyped =
        LastModified (DateTime.Parse ("1994/10/29 19:43:31"))

    let lastModifiedString =
        "Sat, 29 Oct 1994 19:43:31 GMT"

    roundTrip (LastModified.format, LastModified.parse) [
        lastModifiedTyped, lastModifiedString ]

[<Fact>]
let ``ETag Formatting/Parsing`` () =

    let eTagTyped =
        ETag (Strong "sometag")

    let eTagString =
        "\"sometag\""

    roundTrip (ETag.format, ETag.parse) [
        eTagTyped, eTagString ]

[<Fact>]
let ``IfMatch Formatting/Parsing`` () =

    let ifMatchTyped =
        IfMatch (IfMatchChoice.EntityTags [ Strong "sometag"; Weak "othertag" ])

    let ifMatchString =
        "\"sometag\",W/\"othertag\""

    roundTrip (IfMatch.format, IfMatch.parse) [
        ifMatchTyped, ifMatchString ]

[<Fact>]
let ``IfNoneMatch Formatting/Parsing`` () =

    let ifNoneMatchTyped =
        IfNoneMatch (IfNoneMatchChoice.EntityTags [ Strong "sometag"; Weak "othertag" ])

    let ifNoneMatchString =
        "\"sometag\",W/\"othertag\""

    roundTrip (IfNoneMatch.format, IfNoneMatch.parse) [
        ifNoneMatchTyped, ifNoneMatchString ]

[<Fact>]
let ``IfModifiedSince Formatting/Parsing`` () =

    let ifModifiedSinceTyped =
        IfModifiedSince (DateTime.Parse ("1994/10/29 19:43:31"))

    let ifModifiedSinceString =
        "Sat, 29 Oct 1994 19:43:31 GMT"

    roundTrip (IfModifiedSince.format, IfModifiedSince.parse) [
        ifModifiedSinceTyped, ifModifiedSinceString ]

[<Fact>]
let ``IfUnmodifiedSince Formatting/Parsing`` () =

    let ifUnmodifiedSinceTyped =
        IfUnmodifiedSince (DateTime.Parse ("1994/10/29 19:43:31"))

    let ifUnmodifiedSinceString =
        "Sat, 29 Oct 1994 19:43:31 GMT"

    roundTrip (IfUnmodifiedSince.format, IfUnmodifiedSince.parse) [
        ifUnmodifiedSinceTyped, ifUnmodifiedSinceString ]

(* RFC 7233 *)

[<Fact>]
let ``IfRange Formatting/Parsing`` () =

    let ifRangeTyped =
        IfRange (IfRangeChoice.Date (DateTime.Parse ("1994/10/29 19:43:31")))

    let ifRangeString =
        "Sat, 29 Oct 1994 19:43:31 GMT"

    roundTrip (IfRange.format, IfRange.parse) [
        ifRangeTyped, ifRangeString ]

(* RFC 7234 *)

[<Fact>]
let ``Age Formatting/Parsing`` () =

    let ageTyped =
        Age (TimeSpan.FromSeconds (float 1024))

    let ageString =
        "1024"

    roundTrip (Age.format, Age.parse) [
        ageTyped, ageString ]

[<Fact>]
let ``CacheControl Formatting/Parsing`` () =

    let cacheControlTyped =
        CacheControl [
            MaxAge (TimeSpan.FromSeconds (float 1024))
            NoCache
            Private ]

    let cacheControlString =
        "max-age=1024,no-cache,private"

    roundTrip (CacheControl.format, CacheControl.parse) [
        cacheControlTyped, cacheControlString ]

[<Fact>]
let ``Expires Formatting/Parsing`` () =

    let expiresTyped =
        Expires (DateTime.Parse ("1994/10/29 19:43:31"))

    let expiresString =
        "Sat, 29 Oct 1994 19:43:31 GMT"

    roundTrip (Expires.format, Expires.parse) [
        expiresTyped, expiresString ]

[<Fact>]
let ``Authorization Formatting/Parsing`` () =

    let basicAuthTyped =
        Authorization (AuthScheme "Basic", Some (TokenCredentials("abc")))

    let basicAuthString =
        "Basic abc"

    let paramsAuthTyped =
        Authorization (AuthScheme "custom", Some (ParamCredentials ([AuthParam("name", "pete"); AuthParam("role", "admin")])))

    let paramsAuthString =
        "custom name=pete,role=admin"

    roundTrip (Authorization.format, Authorization.parse) [
        basicAuthTyped, basicAuthString ;
        paramsAuthTyped, paramsAuthString ]

[<Fact>]
let ``ProxyAuthorization Formatting/Parsing`` () =

    let basicAuthTyped =
        ProxyAuthorization (AuthScheme "Basic", Some (TokenCredentials("abc")))

    let basicAuthString =
        "Basic abc"

    let paramsAuthTyped =
        ProxyAuthorization (AuthScheme "custom", Some (ParamCredentials ([AuthParam("name", "pete"); AuthParam("role", "admin")])))

    let paramsAuthString =
        "custom name=pete,role=admin"

    roundTrip (ProxyAuthorization.format, ProxyAuthorization.parse) [
        basicAuthTyped, basicAuthString ;
        paramsAuthTyped, paramsAuthString ]


[<Fact>]
let ``WWWAuthenticate Formatting/Parsing`` () =

    let basicAuthTyped =
        WWWAuthenticate [(Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.TokenChallenge("abc"))))]

    let basicAuthString =
        "Basic abc"

    let paramsAuthTyped =
        WWWAuthenticate
            [ Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "simple")])) ]

    let paramsAuthString =
        "Basic realm=simple"

    let multipleParamsAuthTyped =
        WWWAuthenticate
            [ Challenge (AuthScheme "Newauth", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "apps"); AuthParam ("type", "1"); AuthParam ("title", "Login to \"apps\"")]))
            ; Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "simple")])) ]

    let multipleParamsAuthString =
        "Newauth realm=apps,type=1,title=\"Login to \\\"apps\\\"\", Basic realm=simple"

    let multipleMixedAuthTyped =
        WWWAuthenticate
            [ Challenge (AuthScheme "Newauth", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "apps"); AuthParam ("type", "1"); AuthParam ("title", "Login to \"apps\"")]))
            ; Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.TokenChallenge "simple")) ]

    let multipleMixedAuthString =
        "Newauth realm=apps,type=1,title=\"Login to \\\"apps\\\"\", Basic simple"

    roundTrip (WWWAuthenticate.format, WWWAuthenticate.parse) [
        basicAuthTyped, basicAuthString ;
        paramsAuthTyped, paramsAuthString ;
        multipleParamsAuthTyped, multipleParamsAuthString ;
        multipleMixedAuthTyped, multipleMixedAuthString ]

[<Fact>]
let ``ProxyAuthenticate Formatting/Parsing`` () =

    let basicAuthTyped =
        ProxyAuthenticate [(Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.TokenChallenge("abc"))))]

    let basicAuthString =
        "Basic abc"

    let paramsAuthTyped =
        ProxyAuthenticate
            [ Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "simple")])) ]

    let paramsAuthString =
        "Basic realm=simple"

    let multipleParamsAuthTyped =
        ProxyAuthenticate
            [ Challenge (AuthScheme "Newauth", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "apps"); AuthParam ("type", "1"); AuthParam ("title", "Login to \"apps\"")]))
            ; Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "simple")])) ]

    let multipleParamsAuthString =
        "Newauth realm=apps,type=1,title=\"Login to \\\"apps\\\"\", Basic realm=simple"

    let multipleMixedAuthTyped =
        ProxyAuthenticate
            [ Challenge (AuthScheme "Newauth", Some (AuthenticateChallenge.ParamChallenge [AuthParam ("realm", "apps"); AuthParam ("type", "1"); AuthParam ("title", "Login to \"apps\"")]))
            ; Challenge (AuthScheme "Basic", Some (AuthenticateChallenge.TokenChallenge "simple")) ]

    let multipleMixedAuthString =
        "Newauth realm=apps,type=1,title=\"Login to \\\"apps\\\"\", Basic simple"

    roundTrip (ProxyAuthenticate.format, ProxyAuthenticate.parse) [
        basicAuthTyped, basicAuthString ;
        paramsAuthTyped, paramsAuthString ;
        multipleParamsAuthTyped, multipleParamsAuthString ;
        multipleMixedAuthTyped, multipleMixedAuthString ]
