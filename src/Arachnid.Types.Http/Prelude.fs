﻿namespace Arachnid.Types.Http

open System
open Arachnid.Types
open Arachnid.Types.Operators

(* Prelude *)

(* String Extensions *)

[<RequireQualifiedAccess>]
module internal String =

    let equalsCI s1 s2 =
        String.Equals (s1, s2, StringComparison.OrdinalIgnoreCase)

(* Formatting *)

[<RequireQualifiedAccess>]
module Formatting =
    let quotedString (s: string) =
        Formatting.appendC '"'
        >?> (
            s.ToCharArray()
            |> Array.map (
                function
                | '"' | '\\' as c -> Formatting.appendC '\\' >?> Formatting.appendC c
                | c -> Formatting.appendC c
            )
            |> Array.fold (>?>) (Result.Ok)
            )
        >?> Formatting.appendC '"'

    let private tokenExcludedChars = "\"(),/:;<=>?@[\\]{}".ToCharArray()

    let tokenOrQuotedString (s: string) =
        match s.IndexOfAny(tokenExcludedChars) with
        | -1 -> Formatting.append s
        | _  -> quotedString s

(* List Extensions *)

[<RequireQualifiedAccess>]
module internal List =

    let chooseMaxBy projection =
            List.map (fun x -> x, projection x)
         >> List.choose (function | (x, Some y) -> Some (x, y) | _ -> None)
         >> List.sortBy (fun (_, y) -> y)
         >> List.map fst
         >> function | [] -> None
                     | x :: _ -> Some x
