﻿namespace Arachnid.Types

open FParsec

#if DEBUG

module Parsing =
    module Operators =
        let (<!>) (p: Parser<_,_>) label : Parser<_,_> =
            fun stream ->
                printfn "%A: Entering %s" stream.Position label
                let reply = p stream
                printfn "%A: Leaving %s (%A)" stream.Position label reply.Status
                reply

#endif

(* Types *)

type Parse<'a> =
    Parser<'a,unit>
