﻿namespace Arachnid.Types

open System.Globalization
open System.Text

(* Operators *)

module Operators =
  let (>?>) (f1 : 'a -> Result<'b, 'e>) (f2 : 'b -> Result<'c, 'e>)  = f1 >> Result.bind f2

(* Types *)

type FormatResult =
  Result<StringBuilder, string>

type Format<'a> =
    'a -> StringBuilder -> FormatResult

(* Formatting *)

[<RequireQualifiedAccess>]
module Formatting =
    open Operators

    let appendC (s: char) (b: StringBuilder) =
        b.Append (s) |> Ok

    let append (s: string) (b: StringBuilder) =
        b.Append (s) |> Ok

    let appendf1 (s: string) (v1: obj) (b: StringBuilder) =
        b.AppendFormat (CultureInfo.InvariantCulture, s, v1) |> Ok

    let appendf2 (s: string) (v1: obj) (v2: obj) (b: StringBuilder) =
        b.AppendFormat (CultureInfo.InvariantCulture, s, v1, v2) |> Ok

    let join<'a> (f: Format<'a>) (s: StringBuilder -> FormatResult) =
        let rec join values (b: StringBuilder) =
            match values with
            | [] -> Ok b
            | [v] -> f v b
            | v :: vs -> (f v >?> s >?> join vs) b

        join

