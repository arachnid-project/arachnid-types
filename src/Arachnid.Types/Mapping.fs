﻿namespace Arachnid.Types

open System.Text
open FParsec

(* Types *)

type Mapping<'a> =
    { Parse: Parse<'a>
      Format: Format<'a> }

(* Mapping *)

[<RequireQualifiedAccess>]
module Mapping =
    
    let tryFormat (mapping: Mapping<'a>) =
        fun a ->
            Result.map string (mapping.Format a (StringBuilder ()))

    let format (mapping : Mapping<'a>) =
        fun a ->
            match tryFormat mapping a with
            | Result.Ok(s) -> s
            | Result.Error(e) -> failwith e

    let tryParse (mapping: Mapping<'a>) =
        fun s ->
            match run (mapping.Parse .>> eof) s with
            | Success (x, _, _) -> Result.Ok x
            | Failure (e, _, _) -> Result.Error e

    let parse (mapping: Mapping<'a>) =
        fun s ->
            match tryParse mapping s with
            | Result.Ok x -> x
            | Result.Error e -> failwith e
