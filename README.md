# Arachnid Types

## Overview

Arachnid Types provides strongly typed data structures for core web standards (e.g. HTTP, URIs, Language Tags, etc.) as well as appropriate parsers and optics relating to the provided types.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-types/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-types/commits/master)


## See Also

For more information see the [meta-repository for the Arachnid Web Stack](https://gitlab.com/arachnid-project/arachnid), along with the main [arachnid.io](https://arachnid.io) site.
